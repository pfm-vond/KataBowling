﻿using System;
using System.Linq;

namespace kataBowlingV3
{
    public class BowlingGame : IBowlingGame
    {
        private int[] PinsKnockedDown = new int[22];
        private int[] Frames = new int[10];

        private bool IsFirstRoll => Frames[CurrentFrame] == CurrentRoll;
        private int CurrentRoll = 0;
        private int CurrentFrame = 0;

        public bool IsFinish
        {
            get
            {
                bool isFinish = false;

                if (PlayerWinOverTime() && !OverTimedHasBeenPlayed())
                {
                    return false;
                }
                else
                {
                    isFinish = CurrentFrame > LastFrame;
                }

                return isFinish;
            }
        }

        private bool OverTimedHasBeenPlayed()
        {
            return CurrentRoll > Frames[LastFrame] + 2;
        }

        private bool PlayerWinOverTime()
        {
            return (IsSpare(LastFrame) || IsStrike(LastFrame));
        }

        public bool IsOverTime => CurrentFrame == 10;

        public int LastFrame => 9;

        public int Score()
        {
            var score = 0;

            for(int frame = 0 ; frame < Frames.Length ; frame++)
            {
                score += FrameScore(frame);
            }

            return score;
        }

        public void Roll(int pinKnockeddown)
        {
            if(IsOverTime)
            {
                OverTimeRoll(pinKnockeddown);
            }
            else
            {
                FrameRoll(pinKnockeddown);
            }
        }

        private void OverTimeRoll(int pinKnockeddown)
        {
            SaveRoll(pinKnockeddown);
            CurrentRoll++;
        }

        private void FrameRoll(int pinKnockeddown)
        {
            IsRollValid(pinKnockeddown);

            SaveRoll(pinKnockeddown);

            PrepareNextRoll();
        }

        private void SaveRoll(int pinKnockeddown)
        {
            PinsKnockedDown[CurrentRoll] = pinKnockeddown;
        }

        private void IsRollValid(int pinKnockeddown)
        {
            if (PinDownInFrame(CurrentFrame) + pinKnockeddown > 10)
            {
                throw new ArgumentException();
            }
        }

        private void PrepareNextRoll()
        {
            if (IsFrameEnd())
            {
                MoveToNextFrame();
            }
            else
            {
                MoveToSecondRoll();
            }
        }

        private bool IsFrameEnd()
        {
            return !IsFirstRoll || PinDownInFrame(CurrentFrame) == 10;
        }

        private void MoveToNextFrame()
        {
            CurrentFrame++;
            CurrentRoll++;
            if (CurrentFrame < 10)
            {
                Frames[CurrentFrame] = CurrentRoll;
            }
        }

        private void MoveToSecondRoll()
        {
            CurrentRoll++;
        }
        
        private int FrameScore(int frame)
        {
            int FirstRoll = Frames[frame];

            int score = PinDownInFrame(frame);

            if(IsStrike(frame))
            {
                score += AddStrikeBonus(FirstRoll);
            }
            else if (IsSpare(frame))
            {
                score += AddSpareBonus(FirstRoll);
            }

            return score;
        }

        private int AddStrikeBonus(int firstRoll)
        {
            int bonus = 0;
            bonus += PinsKnockedDown[firstRoll + 1];
            bonus += PinsKnockedDown[firstRoll + 2];
            return bonus;
        }

        private bool IsStrike(int frame)
        {
            return PinsKnockedDown[Frames[frame]] == 10;
        }

        private int AddSpareBonus(int firstRoll)
        {
            return PinsKnockedDown[firstRoll + 2];
        }

        private bool IsSpare(int frame)
        {
            return PinDownInFrame(frame) == 10;
        }

        private int PinDownInFrame(int frame)
        {
            if(PinsKnockedDown[Frames[frame]] == 10)
            {
                return 10;
            }

            return PinsKnockedDown[Frames[frame]] + PinsKnockedDown[Frames[frame] + 1];
        }
    }
}
