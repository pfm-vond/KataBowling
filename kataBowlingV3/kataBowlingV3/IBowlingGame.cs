﻿namespace kataBowlingV3
{
    public interface IBowlingGame
    {
        bool IsFinish { get; }
        void Roll(int pinKnockeddown);
        int Score();
    }
}
