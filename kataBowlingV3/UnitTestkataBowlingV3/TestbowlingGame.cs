﻿using Autofac.Extras.Moq;
using kataBowlingV3;
using NUnit.Framework;
using System;

namespace UnitTestkataBowlingV3
{
    [TestFixture]
    public class TestbowlingGame
    {
        [Test]
        public void GameAllZeroHas20Throw()
        {
            using (var mock = AutoMock.GetLoose())
            {
                // ARRANGE
                IBowlingGame aGame = mock.Create<BowlingGame>();
                int nbRoll = 0;

                while (!aGame.IsFinish)
                {
                    aGame.Roll(0);
                    nbRoll++;
                }

                // ASSERT
                Assert.That(nbRoll, Is.EqualTo(20));
            }
        }

        [Test]
        public void GameAllZeroScore0Point()
        {
            using (var mock = AutoMock.GetLoose())
            {
                IBowlingGame aGame = mock.Create<BowlingGame>();
                FinishGameWithAllSameRoll(aGame, 0);

                // ASSERT
                Assert.That(aGame.Score(), Is.EqualTo(0));
            }
        }

        [Test]
        public void GameAllOnesScore20Point()
        {
            using (var mock = AutoMock.GetLoose())
            {
                IBowlingGame aGame = mock.Create<BowlingGame>();
                FinishGameWithAllSameRoll(aGame, 1);

                // ASSERT
                Assert.That(aGame.Score(), Is.EqualTo(20));
            }
        }

        [TestCase(0, 11)]
        [TestCase(5, 6)]
        [TestCase(11, 1)]
        public void MoreThan10PinsKnockedDownInOneFrame(int firstRoll, int secondRoll)
        {
            using (var mock = AutoMock.GetLoose())
            {
                IBowlingGame aGame = mock.Create<BowlingGame>();
                
                // ASSERT
                Assert.That(() => RollFrame(aGame, firstRoll, secondRoll), Throws.Exception.AssignableTo<ArgumentException>());
            }
        }

        [Test]
        public void MissPerfectScoreForOnePin()
        {
            using (var mock = AutoMock.GetLoose())
            {
                IBowlingGame aGame = mock.Create<BowlingGame>();

                for(int i = 0; i < 11; i++)
                {
                    RollStrike(aGame);
                }

                aGame.Roll(9);

                // ASSERT
                Assert.That(aGame.Score(), Is.EqualTo(299));
            }
        }

        [Test]
        public void PerfectScore()
        {
            using (var mock = AutoMock.GetLoose())
            {
                IBowlingGame aGame = mock.Create<BowlingGame>();
                
                FinishGameWithAllSameStrike(aGame);

                // ASSERT
                Assert.That(aGame.Score(), Is.EqualTo(300));
            }
        }

        [Test]
        public void StrikeAtFirstRoll()
        {
            using (var mock = AutoMock.GetLoose())
            {
                IBowlingGame aGame = mock.Create<BowlingGame>();

                RollStrike(aGame);

                FinishGameWithAllSameRoll(aGame, 1);

                // ASSERT
                Assert.That(aGame.Score(), Is.EqualTo(30));
            }
        }

        private void RollStrike(IBowlingGame aGame)
        {
            aGame.Roll(10);
        }

        [Test]
        public void SpareAtFirstRoll()
        {
            using (var mock = AutoMock.GetLoose())
            {
                IBowlingGame aGame = mock.Create<BowlingGame>();

                RollSpare(aGame, 9);

                FinishGameWithAllSameRoll(aGame, 1);

                // ASSERT
                Assert.That(aGame.Score(), Is.EqualTo(29));
            }
        }

        [Test]
        public void RollAllSpare()
        {
            using (var mock = AutoMock.GetLoose())
            {
                IBowlingGame aGame = mock.Create<BowlingGame>();
                
                FinishGameWithAllSameSpare(aGame, 1);

                // ASSERT
                Assert.That(aGame.Score(), Is.EqualTo(11*10));
            }
        }

        private static void RollSpare(IBowlingGame aGame, int firstRoll)
        {
            aGame.Roll(firstRoll);
            aGame.Roll(10 - firstRoll);
        }

        [TestCase(0, 5)]
        [TestCase(5, 4)]
        [TestCase(9, 1)]
        public void LessThan10PinsKnockedDownInOneFrame(int firstRoll, int secondRoll)
        {
            using (var mock = AutoMock.GetLoose())
            {
                IBowlingGame aGame = mock.Create<BowlingGame>();

                // ASSERT
                Assert.That(() => RollFrame(aGame, firstRoll, secondRoll), Throws.Nothing);
            }
        }

        private void RollFrame(IBowlingGame aGame, int firstRoll, int secondRoll)
        {
            aGame.Roll(firstRoll);
            aGame.Roll(secondRoll);
        }

        private IBowlingGame FinishGameWithAllSameSpare(IBowlingGame aGame, int pinsKnocked)
        {
            // ARRANGE

            while (!aGame.IsFinish)
            {
                RollSpare(aGame, pinsKnocked);
            }

            return aGame;
        }

        private IBowlingGame FinishGameWithAllSameStrike(IBowlingGame aGame)
        {
            // ARRANGE

            while (!aGame.IsFinish)
            {
                RollStrike(aGame);
            }

            return aGame;
        }

        private static IBowlingGame FinishGameWithAllSameRoll(IBowlingGame aGame, int pinsKnocked)
        {
            // ARRANGE

            while (!aGame.IsFinish)
            {
                aGame.Roll(pinsKnocked);
            }

            return aGame;
        }
    }
}
